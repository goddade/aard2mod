package itkach.aard2;

import android.app.SearchManager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

public class FloatingArticleCollectionActivity extends ArticleCollectionActivity {

    private static final String PREF_LOC_X = "locX";
    private static final String PREF_LOC_Y = "locY";
    private int currentX;
    private int currentY;
    private int deltaX = 0;
    private int deltaY = 0;
    private WindowManager.LayoutParams wmParams;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActionBar().show();
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        wmParams = getWindow().getAttributes();
        wmParams.dimAmount = 0;
        wmParams.flags = WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL;
        wmParams.height = (int) (displayMetrics.heightPixels * 0.4);
        loadWindowLocPref();
        wmParams.x = currentX;
        wmParams.y = currentY;
        getWindow().setAttributes(wmParams);
        setTittleAction();
    }

    private void setTittleAction() {
        final int actionBarTitleId = getResources().getIdentifier("action_bar_title", "id", "android");
        TextView actionBarTitle = findViewById(actionBarTitleId);
        actionBarTitle.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                if (action == MotionEvent.ACTION_DOWN) {
                    deltaX = (int) (currentX - event.getRawX());
                    deltaY = (int) (currentY - event.getRawY());
                } else if (action == MotionEvent.ACTION_MOVE) {
                    currentX = (int) (event.getRawX() + deltaX);
                    currentY = (int) (event.getRawY() + deltaY);
                    wmParams.x = currentX;
                    wmParams.y = currentY;
                    getWindow().setAttributes(wmParams);
                    setWindowLocPref();
                }
                return true;
            }
        });
    }

    private void loadWindowLocPref() {
        currentX = prefs().getInt(PREF_LOC_X, 0);
        currentY = prefs().getInt(PREF_LOC_Y, 0);
    }

    private void setWindowLocPref() {
        SharedPreferences.Editor editor = prefs().edit();
        editor.putInt(PREF_LOC_X, currentX);
        editor.putInt(PREF_LOC_Y, currentY);
        editor.commit();
    }

    @Override
    void toggleFullScreen() {
        queryInApp();
    }

    private void queryInApp() {
        String word = articleCollectionPagerAdapter.getPrimaryItem().getWebView().getKey();
        Intent intent = new Intent("aard2.lookupfull");
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(SearchManager.QUERY, word);
        startActivity(intent);
        finish();
    }

    @Override
    protected void applyFullScreenPref() {
        //Override, do nothing.
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                queryInApp();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}

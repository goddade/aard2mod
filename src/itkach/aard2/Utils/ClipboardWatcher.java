package itkach.aard2.Utils;

import android.app.SearchManager;
import android.app.Service;
import android.content.ClipboardManager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.IBinder;

public class ClipboardWatcher extends Service {
    private static final String PREF = "clipboard";
    private static final String PREF_LASTDATA = "lastData";
    private ClipboardManager clipboardManager;
    private String lastData;


    @Override
    public void onCreate() {
        super.onCreate();
        clipboardManager = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
        clipboardManager.addPrimaryClipChangedListener(new ClipboardListener());
        lastData = prefs().getString(PREF_LASTDATA, null);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private SharedPreferences prefs() {
        return getSharedPreferences(PREF, MODE_PRIVATE);
    }

    private void setLastDataPref() {
        prefs().edit().putString(PREF_LASTDATA, lastData).commit();
    }

    private void query(String word) {
        Intent intent = new Intent("aard2.lookup");
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(SearchManager.QUERY, word);
        startActivity(intent);
    }

    private class ClipboardListener implements ClipboardManager.OnPrimaryClipChangedListener {

        @Override
        public void onPrimaryClipChanged() {
            CharSequence chars = clipboardManager.getPrimaryClip().getItemAt(0).getText();
            if (chars != null && chars.length() > 0) {
                String data = chars.toString().trim();
                if (lastData == null || !data.equals(lastData)) {
                    query(data);
                    lastData = data;
                    setLastDataPref();
                }
            }
        }
    }
}

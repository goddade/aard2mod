package itkach.aard2.Utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.media.AsyncPlayer;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.Log;

@RequiresApi(api = Build.VERSION_CODES.M)
public class Pronouncer {
    private static final String TAG = Pronouncer.class.getSimpleName();

    static final String PREF = "pronouncer";
    static final String PREF_AUTO_PRONOUNCE = "autoPronounce";
    static final String PREF_URL_FORMAT = "urlFormat";

    public static String YOUDAO_UK = "http://dict.youdao.com/dictvoice?type=1&audio=%s";
    public static String YOUDAO_US1 = "http://dict.youdao.com/dictvoice?type=2&audio=%s";
    public static String YOUDAO_US2 = "http://dict.youdao.com/dictvoice?type=3&audio=%s";

    private Context context;
    private AsyncPlayer player = null;
    private AudioAttributes attributes;
    private AudioManager audioManager;
    private boolean isAutoPronounce;
    private String urlFormat;

    public Pronouncer(Context c) {
        context = c;
        attributes = new AudioAttributes.Builder()
                .setUsage(AudioAttributes.USAGE_ASSISTANCE_ACCESSIBILITY)
                .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
                .build();
        audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);

        //TODO: add them to Settings
        SharedPreferences pref = context.getSharedPreferences(PREF, Context.MODE_PRIVATE);
        isAutoPronounce = pref.getBoolean(PREF_AUTO_PRONOUNCE, false);
        isAutoPronounce = true;
        urlFormat = pref.getString(PREF_URL_FORMAT, YOUDAO_UK);
    }

    public String getUrl(String word) {
        return String.format(urlFormat, word);
    }

    public void pronounce(String word) {
        if (word == null) {
            return;
        }
        if (audioManager.getStreamVolume(AudioManager.STREAM_MUSIC) == 0) {
            Log.i(TAG, "pronounce: system mute.");
            return;
        }
        Uri uri = Uri.parse(getUrl(word));
        if (player == null) {
            player = new AsyncPlayer(TAG);
        } else {
            player.stop();
        }
        try {
            player.play(context, uri, false, attributes);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }

    public void autoPronounce(String word) {
        if (isAutoPronounce) {
            pronounce(word);
        }
    }

    public void stop() {
        player.stop();
    }
}
